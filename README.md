This Postman_Code_Collection Repo contains implementation for validating expected and actual results for different API requests by multiple ways provided in the form of codes and necessary data files which are required for validation. Validation is done by writting test scripts using Javascript, "pm.test" function in "Tests" tab, request body params in "Body" tab, conditions prior to the tests in "Pre-req" tab. Variables such as "Environment variables", "Collection variables" which were used in this project. The repo contains following folders for specific execution & implementation of various requests:

1. Array_Validation :
    Request is being triggered & validated by means of assigning individual variables for response body params,array object in the form of Key-value pair. Traversing & Validating the array object for expected and actual results by using for-loop as conditional loop.

2. Data_Driven :
    The data which is being sent with the request is passed from data file either from CSV file or JSON file. Request body params declared as variables at Collection level and fetched into the request body at run-time. The request is being triggered at Collection level, by uploading the data file with number of iterations to be performed for particular request.

3. Dynamic_Variables :
    The dynamic variables are the variables which are generated when multiple test data is required to test the API. By creating the script in "Pre-req" tab the random data is generated and stored into the variables at Collection level and same data is passed into the request params in "Body" tab. Validation is being done in "Tests" tab by creating test script.

4. Methods :
    Various methods such as GET/PATCH/POST/PUT/DELETE are triggered with necessary params and validation of the actual & expected results done as well as sample reports were generated using Newman CLI for respective methods.

5. Request_Chaining :
    Automating the execution of Triggering multiple request in sequence by creating the script comes under request chaining. After completion of execution & validation of first request will initiate the execution & validation of second requet and so on.

## Data_Files :
    Contains the CSV & JSON files having the data which is to be sent as request parameters in request body while execution of the API request.

## Newman_Commands :
    Contains the Newman commands for execution of the API request by CLI and generating the html & advanced html (html extra) reports.
